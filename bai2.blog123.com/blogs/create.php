<?php
	include('../_connect_db.php');
	session_start();
	//tiến hành kiểm tra là người dùng đã đăng nhập hay chưa
	//nếu chưa, chuyển hướng người dùng ra lại trang đăng nhập
	if (!isset($_SESSION['username'])) {
		header('Location: ../login.php');
	} else {
        echo 'Bạn đang đăng nhập với tài khoản ' . $_SESSION['username'];
    }
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>Form HTML</title>
</head>

<body>
  <form method="POST" action="http://blog123.com/blogs/save.php">
    <table style="border: solid 1px #000">
      <thead>
        <th>Content</th>
        <th>Type</th>
        <th>Input</th>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td></td>
          <td><input type="text" name="title" placeholder="Nhập tiêu đề" required="true"></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td><input type="text" name="description" placeholder="Nhập mô tả" required="true"></input></td>
        </tr>

        <tr>
          <td></td>
          <td></td>
          <td><input type="longtext" name="content" placeholder="Nhập nội dung" required="true"></td>
        </tr>

        <tr>
          <td></td>
          <td></td>
          <td><input type="datetime" name="publish_date" placeholder="Thời gian công bố" required="true"></td>
        </tr>

        <tr>
          <td></td>
          <td></td>
          <td><input type="datetime" name="created_at" placeholder="Thời gian tạo" required="true"></td>
        </tr>

        <tr>
          <td></td>
          <td></td>
          <td><input type="datetime" name="updated_at" placeholder="Thời gian sửa"></td>
        </tr>

        <tr>
          <td></td>
          <td></td>
          <td><input type="number" name="status" placeholder="Trạng thái (nhập 1 2 3 or 4)" required="true"></td>
        </tr>

        <tr>
          <td>categori_id</td>
          <td>checkbox</td>
          <td style="display: flex;flex-direction: column">
            <select name="category_id" id="">
              <?php
                    include('./convert_array_categories.php');
                    foreach ($cates as $cates) {
                        ?>
              <option value="<?=$cates['id'] ?>"><?=$cates['title']?></option>
              <?php
                    }
                ?>
            </select>
          </td>
        </tr>

        <tr>
          <td></td>
          <td></td>
          <td><input type="datetime" name="deleted_at" placeholder="Thời gian xoá"></td>
        </tr>

        <tr>
          <td></td>
          <td></td>
          <td><input type="number" name="viewed" placeholder="Lượt xem" required="true"></td>
        </tr>

        <tr>
          <td></td>
          <td></td>
          <td><input type="number" name="liked" placeholder="Lượt thích" required="true"></td>
        </tr>
      </tbody>
    </table>
    <button type="submit">Save</button>
  </form>
</body>
</html>