<?php
	include('../_connect_db.php');
	session_start();
	//tiến hành kiểm tra là người dùng đã đăng nhập hay chưa
	//nếu chưa, chuyển hướng người dùng ra lại trang đăng nhập
	if (!isset($_SESSION['username'])) {
		header('Location: ../login.php');
	} else {
        echo 'Bạn đang đăng nhập với tài khoản ' . $_SESSION['username'];
    }
?>


<html>
<head>
	<meta charset="UTF-8">
	<title>List</title>
	<link rel="stylesheet" href="../style.css">
</head>
<body>
	<h2>Chúc mừng bạn có username là <span style="color: red"><?=$_SESSION['username'];?></span> đã đăng nhập thành công!</h2>
<!-- danh sách các bài viết chưa xoá -->
	<h2>
		<?php
		$unworn = "SELECT * FROM  blogs WHERE deleted_at is null";
		$unworn_result = mysqli_query($connection, $unworn);
		echo "Số bài viết chưa xoá: ".mysqli_num_rows($unworn_result)."<br>";
		?>
	</h2>
	<h3>Danh sách các bài chưa xoá:</h3>
	<table>
		<thead>
			<tr>
				<th>STT</th>
				<th>Tiêu Đề</th>
				<th>Mô tả</th>
				<th>Nội dung</th>
				<th>Ngày công bố</th>
				<th>Ngày tạo</th>
				<th>Ngày update</th>
				<th>Trạng thái</th>
				<th>category_id</th>
				<th>Ngày xoá</th>
				<th>Lượt xem</th>
				<th>Lượt thích</th>
				<th>Delete</th>
				<th>Update</th>
			</tr>
		</thead>
		<tbody>
			<?php
				include('_fetch_data.php');
			?>
		</tbody>
	</table>
	<br>

<!-- danh sách các bài viết đã xoá -->
	<h2>
		<?php
		$deleted = "SELECT * FROM  blogs WHERE deleted_at is not null";
		$deleted_result = mysqli_query($connection, $deleted);
		echo "Số bài viết đã xoá: ".mysqli_num_rows($deleted_result)."<br>";
		?>
	</h2>
	<h3>Danh sách các bài đã xoá:</h3>
	<table>
		<thead>
			<tr>
				<th>STT</th>
				<th>Tiêu Đề</th>
				<th>Mô tả</th>
				<th>Nội dung</th>
				<th>Ngày công bố</th>
				<th>Ngày tạo</th>
				<th>Ngày update</th>
				<th>Trạng thái</th>
				<th>category_id</th>
				<th>Ngày xoá</th>
				<th>Lượt xem</th>
				<th>Lượt thích</th>
				<th>Restore</th>
				<th>Empty DB</th>
			</tr>
		</thead>
		<tbody>
			<?php
				include('_fetch_data_deleted.php');
			?>
		</tbody>
	</table>
	<br>
	<h2>
	<?php
		echo "Tổng số bài viết: ".(mysqli_num_rows($deleted_result) + mysqli_num_rows($unworn_result))."<br>";
	?>
	</h2>
	<br>
	<a href="http://blog123.com/blogs/create.php" target="_blank">New write</a>
</body>
</html>