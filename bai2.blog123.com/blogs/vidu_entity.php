<?php
$orig = "I'll \"walk\" the <b>dog</b> now";

$a = htmlentities($orig);

$b = html_entity_decode($a);

echo $a . "<br>"; // I'll &quot;walk&quot; the &lt;b&gt;dog&lt;/b&gt; now

echo "echo $b: ".$b."<br>"; // I'll "walk" the <b>dog</b> now
?>