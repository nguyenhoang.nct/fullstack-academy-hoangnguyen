<?php 
$id = isset($_GET['id']) ? $_GET['id'] : 0;
if ($id === 0) {
 header('Location: list.php');
}
?>

<form method="POST" action="update.php">
  <input type="hidden" name="id" value="<?php echo $id ?>" />
  <table style="border: solid 1px #000">
    <thead>
      <th>Content</th>
      <th>Kieu du lieu</th>
      <th>Input</th>
    </thead>
    <tbody>
      <tr>
        <td></td>
        <td></td>
        <td><input type="text" name="title" placeholder="Nhập tiêu đề" required="true"></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td><input name="description" placeholder="Nhập mô tả" required="true"></input></td>
      </tr>

      <tr>
        <td></td>
        <td></td>
        <td><input type="longtext" name="content" placeholder="Nhập nội dung" required="true"></td>
      </tr>

      <tr>
        <td></td>
        <td></td>
        <td><input type="datetime" name="publish_date" placeholder="Thời gian công bố" required="true"></td>
      </tr>

      <tr>
        <td></td>
        <td></td>
        <td><input type="datetime" name="created_at" placeholder="Thời gian tạo" required="true"></td>
      </tr>

      <tr>
        <td></td>
        <td></td>
        <td><input type="datetime" name="updated_at" placeholder="Thời gian sửa"></td>
      </tr>

      <tr>
        <td></td>
        <td></td>
        <td><input type="number" name="status" placeholder="Trạng thái (nhập 1 2 3 or 4)" required="true"></td>
      </tr>

      <tr>
        <td></td>
        <td></td>
        <td><input type="number" name="category_id" placeholder="Nhập category_id" required="true"></td>
			</tr>
			
      <tr>
        <td></td>
        <td></td>
        <td><input type="datetime" name="deleted_at" placeholder="Thời gian xoá"></td>
      </tr>

      <tr>
        <td></td>
        <td></td>
        <td><input type="number" name="viewed" placeholder="Lượt xem" required="true"></td>
      </tr>

      <tr>
        <td></td>
        <td></td>
        <td><input type="number" name="liked" placeholder="Lượt thích" required="true"></td>
      </tr>
    </tbody>
  </table>
  <button type="submit">Save</button>
</form>