<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../_fetch_data_style.css">
</head>
<body>
    <?php
        include('../_connect_db.php');
        $deleted = "SELECT * FROM  blogs WHERE deleted_at is not null";
        $deleted_result = mysqli_query($connection, $deleted);

        // $result = $connection->query($sql);
        // echo "Số lượng bài viết: ".mysqli_num_rows($result);

        while ($post = mysqli_fetch_array($deleted_result, MYSQLI_ASSOC)) {
            echo "<tr>
            <td>".$post["id"]."</td>
            <td>".$post["title"]."</td>
            <td>".$post["description"]."</td>
            <td>".$post["content"]."</td>
            <td>".$post["publish_date"]."</td>
            <td>".$post["created_at"]."</td>
            <td>".$post["updated_at"]."</td>
            <td>".$post["status"]."</td>
            <td>".$post["category_id"]."</td>
            <td>".$post["deleted_at"]."</td>
            <td>".$post["viewed"]."</td>
            <td>".$post["liked"]."</td>
            <td><a href='./restore.php?id=".$post["id"]."' target='_blank'>Restore</a></td>
            <td><a href='./empty_db.php?id=".$post["id"]."' target='_blank'>Empty DB</a></td>
            </tr>";
        }
    ?>
</body>
</html>